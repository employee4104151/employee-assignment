﻿using Newtonsoft.Json;

namespace api.Helpers
{
    public static class JsonFile
    {
        public static T ReadFile<T>(string path)
        {
            var content = System.IO.File.ReadAllText(path);
            return JsonConvert.DeserializeObject<T>(content);
        }

        public static void WriteFile<T>(string path, T data)
        {
            var content = JsonConvert.SerializeObject(data);
            System.IO.File.WriteAllText(path, content);
        }
    }
}

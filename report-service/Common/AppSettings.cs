﻿using Microsoft.Extensions.Configuration;

namespace ReportService.Common
{
    public interface IAppSettings
    {
        string ApiUrl { get; }
        string ReportFolderPath { get; }
        int ReportPeriodInMinute { get; }
    }

    public class AppSettings : IAppSettings
    {
        private readonly IConfiguration configuration;

        public AppSettings(IConfiguration configuration)
        {
            this.configuration = configuration;

            ApiUrl = this.configuration[nameof(ApiUrl)];
            ReportFolderPath = this.configuration[nameof(ReportFolderPath)];
            ReportPeriodInMinute = int.Parse(this.configuration[nameof(ReportPeriodInMinute)]);
        }

        public string ApiUrl { get; }

        public string ReportFolderPath { get; }

        public int ReportPeriodInMinute { get; }

    }
}

﻿using Microsoft.Extensions.DependencyInjection;
using ReportService.Services;

namespace ReportService.Common
{
    public static class ServiceMiddleware
    {
        public static void AddServices(this IServiceCollection services)
        {
            services.AddSingleton<IAppSettings, AppSettings>();
            services.AddSingleton<IEmployeeReportService, EmployeeReportService>();
        }
    }
}

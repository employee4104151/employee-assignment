﻿using api.Helpers;
using api.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ReportService.Common;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ReportService.Services
{
    public interface IEmployeeReportService {
        Task GenerateReport(); 
    }

    public class EmployeeReportService : IEmployeeReportService
    {
        private readonly IHttpClientFactory httpClientFactory;
        private readonly IAppSettings appSetings;
        private readonly ILogger<EmployeeReportService> logger;

        public EmployeeReportService(IHttpClientFactory httpClientFactory, IAppSettings appSetings, ILogger<EmployeeReportService> logger)
        {
            this.httpClientFactory = httpClientFactory;
            this.appSetings = appSetings;
            this.logger = logger;
        }

        public async Task GenerateReport()
        {
            try
            {
                var employees = await GetEmployeeData();
                var filePath = string.Format("{0}/employee_report_{1}.txt", appSetings.ReportFolderPath, DateTime.Now.ToString("MMddyyyy HHmmss"));
                JsonFile.WriteFile(filePath, employees);
            }
            catch (Exception ex) {
                logger.LogError(ex, ex.Message);
            }
        }

        private async Task<IEnumerable<Employee>> GetEmployeeData()
        {
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, string.Format("{0}{1}", appSetings.ApiUrl, "/Employee"));
            var httpClient = httpClientFactory.CreateClient();
            var httpResponseMessage = httpClient.Send(httpRequestMessage);
            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var content = await httpResponseMessage.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<Employee>>(content);
            }

            return new List<Employee>();
        }
    }
}

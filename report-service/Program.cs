using ReportService;
using ReportService.Common;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices(services =>
    {
        services.AddHostedService<Worker>();
        services.AddServices();
        services.AddHttpClient();
    })
    .Build();

await host.RunAsync();

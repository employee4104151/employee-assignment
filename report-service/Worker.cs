using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ReportService.Common;
using ReportService.Services;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ReportService
{
    public class Worker : BackgroundService
    {
        private int executionCount = 0;
        private readonly ILogger<Worker> logger;
        private Timer? timer = null;
        private readonly IEmployeeReportService employeeReportService;
        private readonly IAppSettings appSettings;

        public Worker(ILogger<Worker> logger, IEmployeeReportService employeeReportService, IAppSettings appSettings)
        {
            this.logger = logger;
            this.employeeReportService = employeeReportService;
            this.appSettings = appSettings;
        }       

        private void DoWork(object? state)
        {
            employeeReportService.GenerateReport();
            var count = Interlocked.Increment(ref executionCount);

            logger.LogInformation("GenerateReport: {Count}", count);
        }        

        public override void Dispose()
        {
            timer?.Dispose();
        }       

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromMinutes(appSettings.ReportPeriodInMinute));
            return Task.CompletedTask;
        }
    }
}

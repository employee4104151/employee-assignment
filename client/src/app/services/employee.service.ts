import { Employee } from '../models/employee.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, ReplaySubject, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  private cachedEmployees = new ReplaySubject<Employee[]>(1);

  employees$: Observable<Employee[]> = this.cachedEmployees.asObservable();

  constructor(private http: HttpClient) {}

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error('err', error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private generateHeader() {
    const token = 'some token';
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      }),
    };
  }

  getAll() {
    const url = `${environment.ApiUrl}/Employee`;
    this.http
      .get<Employee[]>(url, this.generateHeader())
      .pipe(
        tap((_) => console.log('getAll')),
        catchError(this.handleError<Employee[]>('getAll', []))
      )
      .subscribe((res) => this.cachedEmployees.next(res));
  }

  get(id: number): Observable<Employee> {
    const url = `${environment.ApiUrl}/Employee/${id}`;
    return this.http
      .get<Employee>(url, this.generateHeader())
      .pipe(catchError(this.handleError<Employee>(`get id=${id}`)));
  }

  create(employee: Employee) {
    const url = `${environment.ApiUrl}/Employee`;
    return this.http
      .post(url, employee, this.generateHeader())
      .pipe(catchError(this.handleError<any>('create')));
  }

  update(employee: Employee): Observable<any> {
    const url = `${environment.ApiUrl}/Employee/${employee.id}`;
    return this.http.put(url, employee, this.generateHeader());
    //.pipe(catchError(this.handleError<any>('update')));
  }

  delete(id: number) {
    const url = `${environment.ApiUrl}/Employee/${id}`;
    return this.http
      .delete(url, this.generateHeader())
      .pipe(catchError(this.handleError<any>('delete')));
  }
}

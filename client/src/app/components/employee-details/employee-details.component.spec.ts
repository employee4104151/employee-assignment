import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { EmployeeDetailsComponent } from './employee-details.component';
import { HttpClientModule } from '@angular/common/http';
import { EmployeeService } from '../../services/employee.service';
import { of } from 'rxjs';

describe('EmployeeDetailsComponent', () => {
  let component: EmployeeDetailsComponent;
  let fixture: ComponentFixture<EmployeeDetailsComponent>;
  let mockEmployeeService;
  const mockedEmployee = {
    id: 123,
    name: 'bob',
    position: 'staff',
    salary: 101,
    hiringDate: new Date('2022-01-01'),
  };

  beforeEach(async () => {
    mockEmployeeService = jasmine.createSpyObj(['get']);
    mockEmployeeService.get.and.returnValue(of(mockedEmployee));
    await TestBed.configureTestingModule({
      imports: [
        EmployeeDetailsComponent,
        RouterModule.forRoot([]),
        HttpClientModule,
      ],
      providers: [{ provide: EmployeeService, useValue: mockEmployeeService }],
    }).compileComponents();

    fixture = TestBed.createComponent(EmployeeDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get employee from service', () => {
    expect(component.employee).toEqual(mockedEmployee);
  });

  it('should show employee', () => {
    const bannerElement: HTMLElement = fixture.nativeElement;
    const employeeDiv = bannerElement.querySelector('.employee-info-box')!;
    expect(employeeDiv.textContent).toEqual(
      'Id: 123Name: bobPosition: staffHiring Date: 2022-01-01 Salary: 101'
    );
  });
});

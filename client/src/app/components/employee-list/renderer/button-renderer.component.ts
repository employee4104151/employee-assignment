// app/button-cell-renderer.component.ts

import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ICellRendererParams } from 'ag-grid-community';

@Component({
  selector: 'btn-cell-renderer',
  template: `
    <button class="btn btn-secondary me-1" (click)="edit($event)">Edit</button>
    <button class="btn btn-danger" (click)="delete($event)">Delete</button>
  `,
  styleUrls: [],
})
export class BtnCellRenderer implements ICellRendererAngularComp {
  private params: any;
  refresh(params: ICellRendererParams<any, any, any>): boolean {
    this.params = params;
    return true;
  }

  agInit(params: any): void {
    this.params = params;
  }

  delete(event: any) {
    this.params.delete(this.params);
  }

  edit(event: any) {
    this.params.edit(this.params);
  }
}

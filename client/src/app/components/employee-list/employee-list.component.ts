import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../services/employee.service';
import { Employee } from '../../models/employee.model';
import { CommonModule } from '@angular/common';
import { RouterLink, Router } from '@angular/router';
import { AgGridModule } from 'ag-grid-angular';
import { ColDef } from 'ag-grid-community';
import { BtnCellRenderer } from './renderer/button-renderer.component';
import { HttpClientModule } from '@angular/common/http';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'employee-list',
  standalone: true,
  imports: [CommonModule, RouterLink, AgGridModule, HttpClientModule],
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss'],
})
export class EmployeeListComponent implements OnInit {
  constructor(
    private employeeService: EmployeeService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.employeeService.getAll();
    this.employeeService.employees$.subscribe((employees) => {
      this.employees = employees;
    });
  }

  employees: Employee[] = [];
  datePipe = new DatePipe('en-US');

  colDefs: ColDef[] = [
    { field: 'id' },
    {
      field: 'name',
      cellRenderer: (params: any) => {
        return `<a href="/employee/${params.data.id}">${params.data.name} </a>`;
      },
    },
    { field: 'position' },
    {
      field: 'hiringDate',
      cellRenderer: (data: any) => {
        return this.datePipe.transform(data.value, 'yyyy-MM-dd');
      },
    },
    { field: 'salary' },
    {
      field: 'actions',
      cellRenderer: BtnCellRenderer,
      cellRendererParams: {
        delete: this.delete.bind(this),
        edit: this.edit.bind(this),
      },
    },
  ];

  delete(field: any) {
    if (confirm('Are you sure to delete ' + field.data.name)) {
      this.employeeService.delete(field.data.id).subscribe((value) => {
        this.getEmployees();
      });
    }
  }

  edit(field: any) {
    this.router.navigate([`update-employee/${field.data.id}`]);
  }

  getEmployees() {
    this.employeeService.getAll();
  }
}

import { Component, OnInit } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { EmployeeService } from '../../services/employee.service';
import { Employee } from '../../models/employee.model';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomValidators } from '../../common/custom-validators';
import { DatePipe } from '@angular/common';

enum Mode {
  Create = 1,
  Edit,
}

@Component({
  selector: 'employee-update',
  standalone: true,
  imports: [ReactiveFormsModule, CommonModule],
  templateUrl: './employee-update.component.html',
  styleUrls: ['./employee-update.component.scss'],
})
export class EmployeeUpdateComponent implements OnInit {
  editingEmployee: Employee | undefined = undefined;
  public title: string = 'create employee';
  mode: Mode = Mode.Create;
  datePipe = new DatePipe('en-US');
  submitedMessage = '';
  submitedClass = '';

  constructor(
    private employeeService: EmployeeService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      if (params['id']) {
        this.mode = Mode.Edit;
        this.title = 'update employee';
        this.employeeService.get(+params['id']).subscribe((value) => {
          this.editingEmployee = value;
          this.updateForm();
        });
      }
    });
  }

  updateForm() {
    this.updateFormControl('name');
    this.updateFormControl('position');
    this.updateFormControl('hiringDate', (date: any) => {
      return this.datePipe.transform(date, 'yyyy-MM-dd');
    });
    this.updateFormControl('salary');
  }

  updateFormControl(field: string, format: any = null) {
    type ObjectKey = keyof typeof this.editingEmployee;
    const nameFormControl = this.form.get(field);
    if (nameFormControl && this.editingEmployee) {
      const value = this.editingEmployee[field as ObjectKey];
      nameFormControl.setValue(format ? format(value) : value);
    }
  }

  form = new FormGroup({
    name: new FormControl('', [Validators.required]),
    position: new FormControl('', []),
    hiringDate: new FormControl('', [
      Validators.required,
      Validators.pattern('[0-9]{4}-[0-9]{2}-[0-9]{2}'),
      CustomValidators.dateValidator,
    ]),
    salary: new FormControl('', [
      Validators.required,
      Validators.pattern('^[0-9]*$'),
      Validators.min(1),
    ]),
  });

  onSubmit() {
    const employee = {
      name: this.form.value.name || '',
      position: this.form.value.position || '',
      hiringDate: new Date(this.form.value.hiringDate || ''),
      salary: +(this.form.value.salary || ''),
      id: this.editingEmployee?.id || 0,
    };

    this.mode === Mode.Create
      ? this.employeeService.create(employee as Employee).subscribe({
          next: this.handleSuccess,
          error: this.handleFailure,
        })
      : this.employeeService.update(employee as Employee).subscribe({
          next: this.handleSuccess,
          error: this.handleFailure,
        });
  }

  handleSuccess = (res: any) => {
    this.submitedMessage = 'Successfully!';
    this.submitedClass = 'success';
    setTimeout(this.goToListPage, 1000);
  };

  handleFailure = (e: any) => {
    this.submitedMessage = 'Failed!';
    this.submitedClass = 'failed';
  };

  goToListPage = () => {
    this.router.navigate(['']);
  };
}

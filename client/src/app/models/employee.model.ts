export interface Employee {
  id: number;
  name: string;
  position: string;
  hiringDate: Date;
  salary: number;
}

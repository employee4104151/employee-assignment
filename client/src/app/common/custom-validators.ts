import { AbstractControl, ValidationErrors } from '@angular/forms';

export class CustomValidators {
  public static dateValidator(
    control: AbstractControl
  ): ValidationErrors | null {
    let hiringDate = control.value;
    if (isNaN(Date.parse(hiringDate))) {
      return {
        date: {
          value: hiringDate,
        },
      };
    }

    return null;
  }
}

﻿using Domain.Entities;
using System.Collections.Generic;

namespace Domain.Services
{
    public interface IEmployeeService
    {
        IEnumerable<Employee> GetAll();
        Employee Get(int id);
        bool Insert(Employee employee);
        bool Update(int id, Employee employee);
        bool Delete(int id);
    }
}

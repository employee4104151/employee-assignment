﻿using Domain.Entities;
using Domain.Repositories;
using Domain.Services;
using System.Collections.Generic;

namespace WebApi.Services
{
    
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository employeeRepository;
            
        public EmployeeService(IEmployeeRepository employeeRepository)
        {
            this.employeeRepository = employeeRepository;
        }

        public bool Delete(int id)
        {
            return employeeRepository.Delete(id);
        }

        public Employee Get(int id)
        {
            return employeeRepository.Get(id);
        }

        public IEnumerable<Employee> GetAll()
        {
            return employeeRepository.GetAll();
        }

        public bool Insert(Employee employee)
        {
            return employeeRepository.Insert(employee);
        }

        public bool Update(int id, Employee employee)
        {
            return employeeRepository.Update(id, employee);
        }
    }
}

﻿using Domain.Entities;
using Domain.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeeController : ControllerBase
    {

        private readonly IEmployeeService employeeService;

        public EmployeeController(IEmployeeService employeeService)
        {
            this.employeeService =  employeeService;
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<Employee> GetAll() {
            return employeeService.GetAll();
        }

        [HttpGet]
        [Route("{id}")]
        public Employee Get(int id)
        {
            return employeeService.Get(id);
        }

        [HttpPost]
        [Route("")]
        public bool Insert(Employee employee)
        {
            return employeeService.Insert(employee);
        }

        [HttpPut]
        [Route("{id}")]
        public bool Update(int id, Employee employee)
        {
            return employeeService.Update(id, employee);
        }

        [HttpDelete]
        [Route("{id}")]
        public bool Delete(int id)
        {
            return employeeService.Delete(id);
        }
    }
}

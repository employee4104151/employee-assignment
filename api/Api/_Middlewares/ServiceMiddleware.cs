﻿using WebApi.Services;
using Domain.Repositories;
using Domain.Services;
using Infrastructure.Common;
using Infrastructure.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace WebApi.Middlewares
{
    public static class ServiceMiddleware
    {
        public static void AddServices(this IServiceCollection services) 
        {
            services.AddScoped<IEmployeeRepository, EmployeeRepository>();
            services.AddScoped<IEmployeeService, EmployeeService>();
            services.AddSingleton<IAppSettings, AppSettings>();
            services.AddSingleton<ICacheManager, CacheManager>();
            services.AddSingleton<IJsonFile, JsonFile>();
        }
    }
}

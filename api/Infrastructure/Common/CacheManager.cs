﻿using Microsoft.Extensions.Caching.Memory;
using System;

namespace Infrastructure.Common
{
    public enum CachedKeys
    {
        Employees,
        Employee
    }

    public interface ICacheManager
    {
        void SetCache<T>(string key, T value, int ExpiredInSeconds = 1);

        T GetCache<T>(string key);
    }

    public class CacheManager : ICacheManager
    {
        private const int DefaultExpiredInSeconds = 1;

        private readonly IMemoryCache cache;

        public CacheManager(IMemoryCache cache)
        {
            this.cache = cache;
        }

        public void SetCache<T>(string key, T value, int ExpiredInSeconds = DefaultExpiredInSeconds)
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions()
                   .SetAbsoluteExpiration(TimeSpan.FromSeconds(DefaultExpiredInSeconds));

            cache.Set(key, value, cacheEntryOptions);
        }

        public T GetCache<T>(string key)
        {
            T value;

            if (cache.TryGetValue(key, out value))
            {
                return value;
            }

            return default;
        }
    }
}

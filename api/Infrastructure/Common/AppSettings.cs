﻿using Microsoft.Extensions.Configuration;

namespace Infrastructure.Common
{
    public interface IAppSettings
    {
        string RepositoryFilePath { get; }
    }

    public class AppSettings : IAppSettings
    {
        private readonly IConfiguration configuration;

        public AppSettings(IConfiguration configuration)
        {
            this.configuration = configuration;

            RepositoryFilePath = this.configuration[nameof(RepositoryFilePath)];
        }

        public string RepositoryFilePath { get; }
    }   
}

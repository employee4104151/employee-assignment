﻿using Newtonsoft.Json;

namespace Infrastructure.Common
{
    public interface IJsonFile
    {
        T ReadFile<T>(string path);

        void WriteFile<T>(string path, T data);
    }


    public class JsonFile : IJsonFile
    {
        public T ReadFile<T>(string path)
        {
            var content = System.IO.File.ReadAllText(path);
            return JsonConvert.DeserializeObject<T>(content);
        }

        public void WriteFile<T>(string path, T data)
        {
            var content = JsonConvert.SerializeObject(data);
            System.IO.File.WriteAllText(path, content);
        }
    }
}

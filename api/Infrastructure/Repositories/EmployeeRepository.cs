﻿using Domain.Entities;
using Domain.Repositories;
using Infrastructure.Common;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Repositories
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly IAppSettings appSetting;
        private readonly ILogger<EmployeeRepository> logger;
        private readonly ICacheManager cacheManager;
        private readonly IJsonFile jsonFile;

        public EmployeeRepository(IAppSettings appSetting, ILogger<EmployeeRepository> logger, ICacheManager cacheManager, IJsonFile jsonFile)
        {
            this.appSetting = appSetting;
            this.logger = logger;
            this.cacheManager = cacheManager;
            this.jsonFile = jsonFile;
        }

        public IEnumerable<Employee> GetAll()
        {
            logger.LogInformation("GetAll");
            var employees = jsonFile.ReadFile<IEnumerable<Employee>>(appSetting.RepositoryFilePath);

            return employees;
        }

        public Employee Get(int id)
        {
            var cachedKey = string.Format("{0}{1}", CachedKeys.Employee.ToString(), id.ToString());
            var employee = cacheManager.GetCache<Employee>(cachedKey);
            if (employee == default)
            {
                employee = GetAll().Where(e => e.Id == id).FirstOrDefault();
                cacheManager.SetCache(cachedKey, employee);
            }

            return employee;
        }

        private int GenerateId(List<Employee> employees)
        {
            return employees.Count
                > 0 ? employees.Last().Id + 1 : 1;
        }

        public bool Insert(Employee employee)
        {
            var allEmployees = GetAll().ToList();
            if (allEmployees.FirstOrDefault(e => e.Id == employee.Id) == default)
            {
                employee.Id = GenerateId(allEmployees);
                allEmployees.Add(employee);
                jsonFile.WriteFile<IEnumerable<Employee>>(appSetting.RepositoryFilePath, allEmployees);

                return true;
            }

            return false;
        }

        public bool Update(int id, Employee employee)
        {
            var allEmployees = GetAll().ToList();
            var existingEmployee = allEmployees.FirstOrDefault(e => e.Id == id);
            if (existingEmployee != default)
            {
                existingEmployee.Name = employee.Name;
                existingEmployee.Position = employee.Position;
                existingEmployee.HiringDate = employee.HiringDate;
                existingEmployee.Salary = employee.Salary;
                jsonFile.WriteFile<IEnumerable<Employee>>(appSetting.RepositoryFilePath, allEmployees);

                return true;
            }

            return false;
        }

        public bool Delete(int id)
        {
            var allEmployees = GetAll().ToList();
            var existingEmployee = allEmployees.FirstOrDefault(e => e.Id == id);
            if (existingEmployee != default)
            {
                allEmployees.Remove(existingEmployee);
                jsonFile.WriteFile<IEnumerable<Employee>>(appSetting.RepositoryFilePath, allEmployees);

                return true;
            }

            return false;
        }
    }
}

﻿using Domain.Entities;
using Infrastructure.Common;
using Infrastructure.Repositories;
using Microsoft.Extensions.Logging;
using NSubstitute;
using System.Collections.Generic;
using Xunit;

namespace UnitTests.Repositories
{
    public class EmployeeRepositoryTests
    {
        private readonly IAppSettings appSetting;
        private readonly ILogger<EmployeeRepository> logger;
        private readonly ICacheManager cacheManager;
        private readonly IJsonFile jsonFile;

        public EmployeeRepositoryTests()
        {
            appSetting = Substitute.For<IAppSettings>();
            logger = Substitute.For<ILogger<EmployeeRepository>>();
            cacheManager = Substitute.For<ICacheManager>();
            jsonFile = Substitute.For<IJsonFile>();
        }

        [Fact]
        public void Get_ValidCache_ReturnEmployeeFromCache()
        {
            // Arrange
            var id = 11;
            var employee = new Employee() { Id = 11, Name = "e1" };
            var cachedKey = "Employee11";
            cacheManager.GetCache<Employee>(cachedKey).Returns(employee);
            var repository = new EmployeeRepository(appSetting, logger, cacheManager, jsonFile);

            // Act
            var result = repository.Get(id);

            // Assert
            Assert.Equal(employee, result);
        }

        [Fact]
        public void Get_NotCached_ReturnEmployeeFromJsonFile()
        {
            // Arrange
            var id = 11;
            var employees = new List<Employee>() {
                new Employee() { Id = 10, Name = "e0" },
                new Employee() { Id = 11, Name = "e1" } 
            };
            var cachedKey = "Employee11";
            cacheManager.GetCache<Employee>(cachedKey).Returns(v => null);
            jsonFile.ReadFile<IEnumerable<Employee>>(appSetting.RepositoryFilePath).Returns(employees);
            var repository = new EmployeeRepository(appSetting, logger, cacheManager, jsonFile);

            // Act
            var result = repository.Get(id);

            // Assert
            Assert.Equal(employees[1], result);
        }
    }
}

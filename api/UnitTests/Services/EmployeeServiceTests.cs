﻿using Domain.Entities;
using Domain.Repositories;
using NSubstitute;
using System.Collections.Generic;
using WebApi.Services;
using Xunit;

namespace XUnitTests.Services
{
    public class EmployeeServiceTests
    {
        [Fact]
        public void GetAll_WhenCalled_ReturnResultFromRespository()
        {
            // Arrange
            var repository = Substitute.For<IEmployeeRepository>();
            var employees = new List<Employee>() {
                new Employee(){ Id = 1, Name = "e1"}
            };
            repository.GetAll().Returns(employees);
            var service = new EmployeeService(repository);

            // Act
            var result = service.GetAll();

            // Assert
            Assert.Equal(employees, result);
        }
    }
}
